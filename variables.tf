variable "ssh_port" {
  description = "SSH Port"
  type        = number
  default     = 22
}

variable "instance_type" {
  description = "Instance Type"
  type        = string
  default     = "t2.micro"
}

variable "ami" {
  description = "AMI of Instance"
  type        = string
  default     = "ami-0742b4e673072066f"

}
